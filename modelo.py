import unittest


class ExampleTestCase(unittest.TestCase):
    def test_1_plus_1_equals_two(self):
        result = 1 + 1
        self.assertEqual(result, 2)


if __name__ == '__main__':
    unittest.main()
