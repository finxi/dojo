import unittest


def intervaluator(numbers):
    splitted = numbers.split(' ')
    return buildInterval('', splitted)


def buildInterval(previous, sequence):
    if len(sequence) == 0:
        return previous

    prefix = ''

    if previous != '':
        prefix = previous + ','

    if len(sequence) == 1:
        return prefix + '[' + sequence[0] + ']'

    if len(sequence) == 2:
        if (isSequential(sequence[0], sequence[1])):
            return prefix + '[' + sequence[0] + '-' + sequence[1] + ']'
        return prefix + '[' + sequence[0] + '],[' + sequence[1] +']'
    else:
        result = '['
        finalI = 0
        for i, v in enumerate(sequence):
            if i == 0:
                result += v
            elif not isSequential(sequence[i-1],v):
                result += '-'+ sequence[i-1] + ']'
                finalI = i
                break

        return buildInterval(result, sequence[finalI:len(sequence)])


def isSequential(num1, num2):
    return int(num2) - int(num1) == 1


class IntervalTestCase(unittest.TestCase):
    def test_1_list(self):
        result = intervaluator('1')
        self.assertEqual(result, '[1]')

    def test_2_list(self):
        result = intervaluator('2')
        self.assertEqual(result, '[2]')

    def test_3_list(self):
        result = intervaluator('3')
        self.assertEqual(result, '[3]')

    def test_10_list(self):
        result = intervaluator('10')
        self.assertEqual(result, '[10]')

    def test_1_2_list(self):
        result = intervaluator('1 2')
        self.assertEqual(result, '[1-2]')

    def test_1_3_list(self):
        result = intervaluator('1 3')
        self.assertEqual(result, '[1],[3]')

    def test_1_2_3_list(self):
        result = intervaluator('1 2 3')
        self.assertEqual(result, '[1-3]')

    def test_1_2_4_list(self):
        result = intervaluator('1 2 4')
        self.assertEqual(result, '[1-2],[4]')

    def test_1_3_4_list(self):
        result = intervaluator('1 3 4')
        self.assertEqual(result, '[1],[3-4]')


class IsSequentialTestCase(unittest.TestCase):
    def test_1_3(self):
        result = isSequential('1', '3')
        self.assertEqual(result, False)

    def test_1_2(self):
        result = isSequential('1', '2')
        self.assertEqual(result, True)


if __name__ == '__main__':
    unittest.main()
