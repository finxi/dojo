import unittest
from copy import deepcopy

def nextStep(state):
    alives = sum([sum(x) for x in state])

    new_state = deepcopy(state)

    for x in range(0, len(state)):
        for y in range(0, len(state[0])):
            neighbors = countAliveNeighbors(state, x, y)
            if state[x][y] == 0:
                if neighbors == 3:
                    new_state[x][y] = 1
            else:
                if neighbors < 2 or neighbors > 3:
                    new_state[x][y] = 0
    

    return new_state


def emptyState():
   return [
            [0, 0, 0, 0,],
            [0, 0, 0, 0,],
            [0, 0, 0, 0,],
            [0, 0, 0, 0]
        ]

def countAliveNeighbors(state, x, y):

    total = 0
    if y > 0:
        total+= state[x][y-1]
    if y < len(state) - 1:    
        total+= state[x][y+1]
    if x > 0:
        total += state[x-1][y]
    if x < len(state) - 1:
        total += state[x+1][y]
    if x > 0 and y < len(state) -1: 
        total += state[x-1][y+1]
    if x > 0 and y > 0: 
        total += state[x-1][y-1]
    if x < len(state) - 1 and y < len(state) - 1: 
        total += state[x+1][y+1]
    if x < len(state) -1 and y > 0:
        total += state[x+1][y-1]
    return total


class NeighborCountTestCase(unittest.TestCase):
    def test_0_neighbors(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 0, 0,],
                    [0, 1, 0, 0,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(0, countAliveNeighbors(state, 2, 1))

    def test_1_horizontal_neighbor(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 0, 0,],
                    [0, 1, 1, 0,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 2, 1))

    def test_1_horizontal_with_a_stray_neighbor(self):
        state = [
                    [0, 0, 0, 1,],
                    [0, 0, 0, 0,],
                    [0, 1, 1, 0,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 2, 1))

    def test_1_horizontal_neighbor_out_of_bounds(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 0, 0,],
                    [1, 1, 0, 0,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 2, 0))

    def test_1_horizontal_with_a_stray_neighbor_out_of_bounds(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 0, 0,],
                    [1, 1, 0, 1,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 2, 0))

    def test_1_horizontal_with_a_stray_neighbor_out_of_bounds_on_the_other_side(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 0, 0,],
                    [1, 0, 1, 1,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 2, 3))

    def test_1_vertical_neighbor(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 1, 0,],
                    [0, 0, 1, 0,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 1, 2))

    def test_1_vertical_with_a_neighbor_out_of_bounds(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 0, 0,],
                    [0, 0, 1, 0,],
                    [0, 0, 1, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 3, 2))

    def test_1_diagonal_upper_right(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 1, 0,],
                    [0, 1, 0, 0,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 2, 1))

    def test_1_diagonal_upper_left(self):
        state = [
                    [0, 0, 0, 0,],
                    [1, 0, 0, 0,],
                    [0, 1, 0, 0,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 2, 1))

    def test_1_diagonal_bottom_right(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 0, 0,],
                    [0, 1, 0, 0,],
                    [0, 0, 1, 0]
                ]
        self.assertEqual(1, countAliveNeighbors(state, 2, 1))

    def test_1_real_case(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 1, 0, 0,],
                    [0, 1, 1, 0,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(3, countAliveNeighbors(state, 1, 2))



class GameOfLifeTestCase(unittest.TestCase):
    def test_no_cells_is_dead(self):
        state =  emptyState()
        result = nextStep(state)
        self.assertEqual(emptyState(), result)

    def test_single_cell_dies(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 0, 0, 0,],
                    [0, 1, 0, 0,],
                    [0, 0, 0, 0]
                ]
        result = nextStep(state)
        self.assertEqual(emptyState(), result)

    def test_stable_state(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 1, 1, 0,],
                    [0, 1, 1, 0,],
                    [0, 0, 0, 0]
                ]
        result = nextStep(state)
        self.assertEqual(state, result)

    def test_unstable_state(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 1, 0, 0,],
                    [0, 1, 0, 0,],
                    [0, 0, 0, 0]
                ]
        result = nextStep(state)
        self.assertEqual(emptyState(), result)

    def test_forever_alones_state(self):
        state = [
                    [1, 0, 0, 0,],
                    [0, 0, 0, 0,],
                    [0, 1, 0, 0,],
                    [0, 0, 0, 1]
                ]
        result = nextStep(state)
        self.assertEqual(emptyState(), result)

    def test_new_life_state(self):
        state = [
                    [0, 0, 0, 0,],
                    [0, 1, 0, 0,],
                    [0, 1, 1, 0,],
                    [0, 0, 0, 0]
                ]
        result = nextStep(state)
        next_state = [
                    [0, 0, 0, 0,],
                    [0, 1, 1, 0,],
                    [0, 1, 1, 0,],
                    [0, 0, 0, 0]
                ]
        self.assertEqual(next_state, result)

if __name__ == '__main__':
    unittest.main()
