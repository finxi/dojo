import unittest

boolean_dict = {'true': True, 'false': False}

def evaluate_expression(expression):
    array = expression.split(' ')

    last_value = True
    for i, value in enumerate(array):
        if i % 2 != 0:
            continue
        current_value = boolean_dict[value]
        if i == 0:
            oper = 'and'
        else:
            oper = array[i-1]
        last_value = apply_oper(last_value, current_value, oper)

    return last_value


def apply_oper(last, curr, oper):
    if (oper == 'and'):
        return (last and curr)
    elif (oper == 'xor'):
        return (last != curr)
    return (last or curr)


class ParseBooleanTestCase(unittest.TestCase):
    def test_false_returns_False(self):
        result = evaluate_expression('false')
        self.assertEqual(False, result)

    def test_true_returns_True(self):
        result = evaluate_expression('true')
        self.assertEqual(True, result)

    def test_true_and_false_returns_False(self):
        result = evaluate_expression('true and false')
        self.assertEqual(False, result)

    def test_true_and_true_returns_True(self):
        result = evaluate_expression('true and true')
        self.assertEqual(True, result)

    def test_true_or_false_returns_True(self):
        result = evaluate_expression('true or false')
        self.assertEqual(True, result)

    def test_true_xor_true_returns_False(self):
        result = evaluate_expression('true xor true')
        self.assertEqual(False, result)

    def test_true_and_true_and_true_return_True(self):
        result =  evaluate_expression('true and true and true')
        self.assertEqual(True, result)

    def test_true_xor_false_returns_True(self):
        result = evaluate_expression('true xor false')
        self.assertEqual(True, result)

    def test_true_and_false_or_true_return_True(self):
        result =  evaluate_expression('true and false or true')
        self.assertEqual(True, result)

    def test_true_and_false_xor_true_return_True(self):
        result =  evaluate_expression('true and false xor true')
        self.assertEqual(True, result)

    def test_true_or_true_xor_false_return_True(self):
        result =  evaluate_expression('true or true xor false')
        self.assertEqual(True, result)

if __name__ == '__main__':
    unittest.main()
