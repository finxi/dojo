import unittest
import re


class ParsedExpression:
    def __init__(self, expression):

        self.expression = ParsedExpression.break_expression(expression)

    @staticmethod
    def is_number(num):
        try:
            int(num)
            return True
        except:
            return False

    @staticmethod
    def break_expression(expression):
        regex = re.compile('[\\+\\-\\*/]|\\d+')
        return regex.findall(expression)


class Calc:
    def __init__(self, expression):
        self.result = 0
        self.operators = {"+": self.sum, "-": self.subtract}
        self.result = self.solve_expression(expression)

    def solve_expression(self, expression):
        expression = ParsedExpression.break_expression(expression)

        acc = int(expression[0])
        operator = ''

        for item in expression[1:]:
            if ParsedExpression.is_number(item):
                acc = self.operators[operator](acc, int(item))
            else:
                operator = item

        return acc

        
    def sum(self, operand_1, operand_2):
        return operand_1 + operand_2

    def subtract(self, operand_1, operand_2):
        return operand_1 - operand_2

    def negativeNumber(self, operands):
        pass


class ExpressionResultsTestCase(unittest.TestCase):
    def test_3_equals_3(self):
        expression = "3"
        self.assertEqual(Calc(expression).result, 3)

    def test_20_equals_20(self):
        expression = "20"
        self.assertEqual(Calc(expression).result, 20)

    def test_1_plus_1_equals_2(self):
        expression = "1+1"
        self.assertEqual(Calc(expression).result, 2)

    def test_2_plus_2_equals_4(self):
        expression = "2+2"
        self.assertEqual(Calc(expression).result, 4)

    def test_2_plus_2_plus_2_equals_6(self):
        expression = "2+2+2"
        self.assertEqual(Calc(expression).result, 6)

    def test_2_plus_2_plus_4_equals_8(self):
        expression = "2+2+4"
        self.assertEqual(Calc(expression).result, 8)

    def test_2_minus_2_equals_0(self):
        expression = "2-2"
        self.assertEqual(Calc(expression).result, 0)

    def test_2_minus_2_minus_2_equals_negative_2(self):
        expression = "2-2-2"
        self.assertEqual(Calc(expression).result, -2)

    def test_2_minus_2_minus_2_minus_2_equals_negative_4(self):
        expression = "2-2-2-2"
        self.assertEqual(Calc(expression).result, -4)

    def test_2_minus_2_plus_2_equals_2(self):
        expression = "2-2+2"
        self.assertEqual(Calc(expression).result, 2)

    def test_3_minus_2_plus_2_equals_3(self):
        expression = "3-2+2"
        self.assertEqual(Calc(expression).result, 3)


class ParsedExpressionTestCases(unittest.TestCase):
    def test_detect_correct_operator(self):
        op = ParsedExpression("1+1")
        self.assertEqual(op.expression, ['1','+','1'])

    def test_detect_correct_operands(self):
        op = ParsedExpression("2+5")
        self.assertEqual(op.expression, ['2','+','5'])

if __name__ == '__main__':
    unittest.main()
