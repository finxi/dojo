import unittest


class Calc:

    def __init__(self, expression):
        self.result = 0
        self.expression = expression
        self.operators = {"+": self.sum, "-": self.subtract}

        if "+" in expression and "-" in expression:
            self.result = 2
        elif "+" in expression:
            operands = self.break_expression('+')
            self.result = self.operators['+'](operands)
        elif "-" in expression:
            operands = self.break_expression('-')
            self.result = self.operators['-'](operands)
        else:
            self.result = int(expression)

    def break_expression(self, operator):
        return self.expression.split(operator)

    def sum(self, operands):
        result = 0
        for num in operands:
            result += int(num)
        return result

    def subtract(self, operands):
        result = int(operands[0])
        for i in range(1, len(operands)):
            result -= int(operands[i])

        return result


class ExpressionResultsTestCase(unittest.TestCase):
    def test_3_equals_3(self):
        expression = "3"
        self.assertEqual(Calc(expression).result, 3)

    def test_20_equals_20(self):
        expression = "20"
        self.assertEqual(Calc(expression).result, 20)

    def test_1_plus_1_equals_2(self):
        expression = "1+1"
        self.assertEqual(Calc(expression).result, 2)

    def test_2_plus_2_equals_4(self):
        expression = "2+2"
        self.assertEqual(Calc(expression).result, 4)

    def test_2_plus_2_plus_2_equals_6(self):
        expression = "2+2+2"
        self.assertEqual(Calc(expression).result, 6)

    def test_2_plus_2_plus_4_equals_8(self):
        expression = "2+2+4"
        self.assertEqual(Calc(expression).result, 8)

    def test_2_minus_2_equals_0(self):
        expression = "2-2"
        self.assertEqual(Calc(expression).result, 0)

    def test_2_minus_2_minus_2_equals_negative_2(self):
        expression = "2-2-2"
        self.assertEqual(Calc(expression).result, -2)

    def test_2_minus_2_minus_2_minus_2_equals_negative_4(self):
        expression = "2-2-2-2"
        self.assertEqual(Calc(expression).result, -4)

    def test_2_minus_2_plus_2_equals_2(self):
        expression = "2-2+2"
        self.assertEqual(Calc(expression).result, 2)


if __name__ == '__main__':
    unittest.main()
