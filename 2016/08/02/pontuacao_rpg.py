import unittest
import copy


class Bonus:

    def __init__(self):
        self.items = {}
        self.added = False

    def add(self, attribute, name, value):
        bonus = copy.deepcopy(self)
        if attribute not in bonus.items:
            bonus.items[attribute] = {}

        if name not in bonus.items[attribute]:
            bonus.items[attribute][name] = []

        bonus.items[attribute][name].append(value)
        return bonus

    def total(self, attribute):
        ret = 0

        if attribute in self.items:
            for name in self.items[attribute]:
                if name:
                    ret += max(self.items[attribute][name])
                else:
                    ret += sum(self.items[attribute][name])

        return ret


class AddBonusTestCase(unittest.TestCase):

    def test_save_and_read_bonus(self):
        bonus = Bonus()
        bonus = bonus.add('str', 'nome', 1)
        bonus_total = bonus.total('str')
        self.assertEqual(bonus_total, 1)

    def test_empty_bonus_total_0(self):
        bonus = Bonus()
        total = bonus.total("str")
        self.assertEqual(0, total)

    def test_other_attribute_is_0(self):
        bonus = Bonus()
        bonus = bonus.add("str", "", 1)
        bonus_total = bonus.total('cha')
        self.assertEqual(0, bonus_total)

    def test_save_and_read_multiple_times_same_attribute(self):
        bonus = Bonus()
        bonus = bonus.add("str", "", 1)
        bonus = bonus.add("str", "", 1)
        bonus_total = bonus.total("str")
        self.assertEqual(2, bonus_total)

    def test_save_bonus_with_name(self):
        bonus = Bonus()
        bonus = bonus.add('str', 'sagrado', 1)
        bonus = bonus.add('str', 'sagrado', 2)
        bonus_total = bonus.total('str')

        self.assertEqual(2, bonus_total)

    def test_save_bonus_with_name_inverse(self):
        bonus = Bonus()
        bonus = bonus.add('str', 'sagrado', 2)
        bonus = bonus.add('str', 'sagrado', 1)
        bonus_total = bonus.total('str')

        self.assertEqual(2, bonus_total)

    def test_save_bonus_with_multiple_names(self):
        bonus = Bonus()
        bonus = bonus.add('str', '', 1)
        bonus = bonus.add('str', 'sagrado', 2)
        bonus_total = bonus.total('str')

        self.assertEqual(3, bonus_total)

    def test_immutable(self):
        bonus = Bonus()
        bonus.add('str', '', 1)

        self.assertEqual(0, bonus.total('str'))

    def test_save_multiple_names(self):
        bonus = Bonus()
        bonus = bonus.add('str', 'sagrado', 2)
        bonus = bonus.add('str', 'sagrado', 3)
        bonus = bonus.add('str', 'profano', 3)
        bonus = bonus.add('str', 'profano', 2)

        self.assertEqual(6, bonus.total('str'))


if __name__ == '__main__':
    unittest.main()
