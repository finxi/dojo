import unittest


def look_and_say(number):
    counter = 0
    prev_num = None
    result = ''

    for num in number:
        if prev_num and prev_num != num:
            result += str(counter)
            result += str(prev_num)
            counter = 0
        counter += 1
        prev_num = num

    result += str(counter)
    result += str(prev_num)
    return result


def look_and_say_recursive(number):
    result = ''
    for block in same_numbers(number):
        if not block:
            continue
        result += str(len(block)) + block[0]
    return result

def same_numbers(entry):
    if not entry:
        return []

    last_num = None
    first_block = ''

    for num in entry:
        if not last_num or num == last_num:
            last_num = num
            first_block += num

    leftovers = entry[len(first_block):]
    blocks = [first_block]
    blocks.extend(same_numbers(entry))
    return blocks


class LookAndSayTestCase(unittest.TestCase):
    def test_1_returns_11(self):
        result = look_and_say("1")
        self.assertEqual(result, "11")

    def test_2_returns_12(self):
        result = look_and_say("2")
        self.assertEqual(result, "12")

    def test_3_returns_13(self):
        result = look_and_say('3')
        self.assertEqual(result, '13')

    def test_11_return_21(self):
        result = look_and_say('11')
        self.assertEqual(result, '21')

    def test_22_return_22(self):
        result = look_and_say('22')
        self.assertEqual(result, '22')

    def test_111_return_31(self):
        result = look_and_say('111')
        self.assertEqual(result, '31')

    def test_1111_return_41(self):
        result = look_and_say('1111')
        self.assertEqual(result, '41')

    def test_12_return_1112(self):
        result = look_and_say('12')
        self.assertEqual(result, '1112')

    def test_21_return_1211(self):
        result = look_and_say('21')
        self.assertEqual(result, '1211')

    def test_121_return_111211(self):
        result = look_and_say('121')
        self.assertEqual(result, '111211')

    def test_11231_return_21121311(self):
        result = look_and_say('11231')
        self.assertEqual(result, '21121311')

class LookAndSayRecursiveTestCase(unittest.TestCase):
    def test_1_returns_11(self):
        result = look_and_say_recursive("1")
        self.assertEqual(result, "11")

    def test_2_returns_12(self):
        result = look_and_say_recursive("2")
        self.assertEqual(result, "12")

    def test_3_returns_13(self):
        result = look_and_say_recursive('3')
        self.assertEqual(result, '13')

    def test_11_return_21(self):
        result = look_and_say_recursive('11')
        self.assertEqual(result, '21')

    def test_22_return_22(self):
        result = look_and_say_recursive('22')
        self.assertEqual(result, '22')

    def test_111_return_31(self):
        result = look_and_say_recursive('111')
        self.assertEqual(result, '31')

    def test_1111_return_41(self):
        result = look_and_say_recursive('1111')
        self.assertEqual(result, '41')

    def test_12_return_1112(self):
        result = look_and_say_recursive('12')
        self.assertEqual(result, '1112')

    def test_21_return_1211(self):
        result = look_and_say_recursive('21')
        self.assertEqual(result, '1211')

    def test_121_return_111211(self):
        result = look_and_say_recursive('121')
        self.assertEqual(result, '111211')

    def test_11231_return_21121311(self):
        result = look_and_say_recursive('11231')
        self.assertEqual(result, '21121311')

if __name__ == '__main__':
    unittest.main()
