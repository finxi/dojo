import unittest


class Romano:
    global romanoLetters
    romanoLetters = {
        '': 0, 'I': 1, 'V': 5,
        'X': 10, 'L': 50, 'C': 100,
        'D': 500, 'M': 1000
    }

    def __init__(self, romano):
        self.romano = romano
        self.inteiro = self.fromRomano()

    # def isValid(self):

    def fromRomano(self):
        result = 0
        previous = ""

        for char in reversed(self.romano):
            if romanoLetters[previous] > romanoLetters[char]:
                result -= romanoLetters[char]
            else:
                result += romanoLetters[char]
            previous = char
        return result


class PassinhoDoRomanoTestCase(unittest.TestCase):
    def test_i_equals_1(self):
        result = Romano('I')
        self.assertEqual(result.inteiro, 1)

    def test_v_equals_5(self):
        result = Romano('V')
        self.assertEqual(result.inteiro, 5)

    def test_x_equals_10(self):
        result = Romano('X')
        self.assertEqual(result.inteiro, 10)

    def test_l_equals_50(self):
        result = Romano('L')
        self.assertEqual(result.inteiro, 50)

    def test_c_equals_100(self):
        result = Romano('C')
        self.assertEqual(result.inteiro, 100)

    def test_d_equals_500(self):
        result = Romano('D')
        self.assertEqual(result.inteiro, 500)

    def test_m_equals_1000(self):
        result = Romano('M')
        self.assertEqual(result.inteiro, 1000)

    def test_ii_equals_2(self):
        result = Romano('II')
        self.assertEqual(result.inteiro, 2)

    def test_iv_equals_4(self):
        result = Romano('IV')
        self.assertEqual(result.inteiro, 4)

    def test_liv_equals_54(self):
        result = Romano('LIV')
        self.assertEqual(result.inteiro, 54)

    # def test_iiii_raise_exception(self):
    #    self.assertRaises(ValueError, lambda : Romano('IIII'))


if __name__ == '__main__':
    unittest.main()
