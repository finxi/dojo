import unittest


def numero_feliz(num, lista=None):
    lista = lista or set()
    lista.add(num)
    resposta = sum_array(split_number(num))
    if resposta == 1:
        return True
    elif resposta in lista:
        return False
    else:
        return numero_feliz(resposta, lista)


def split_number(num):
    strNum = str(num)
    for number in strNum:
        yield int(number)


def sum_array(nums):
    total = 0
    for number in nums:
        total = total + number ** 2
    return total


class NumeroFelizTestCase(unittest.TestCase):
    def test_1_is_happy(self):
        self.assertTrue(numero_feliz(1))

    def test_7_is_happy(self):
        self.assertTrue(numero_feliz(7))

    def test_2_is_not_happy(self):
        self.assertFalse(numero_feliz(2))

    def test_10_is_happy(self):
        self.assertTrue(numero_feliz(10))


class SplitNumberTestCase(unittest.TestCase):
    def test_1_should_return_itself(self):
        resposta = split_number(1)
        self.assertIn(1, resposta)

    def test_10_should_return_1_0(self):
        resposta = split_number(10)
        self.assertIn(1, resposta)
        self.assertIn(0, resposta)


class SumArrayTestCase(unittest.TestCase):
    def test_1_should_return_itself(self):
        numbers = split_number(1)
        self.assertEqual(sum_array(numbers), 1)

    def test_10_should_return_itself(self):
        numbers = split_number(10)
        self.assertEqual(sum_array(numbers), 1)

    def test_7_should_return_itself(self):
        numbers = split_number(7)
        self.assertEqual(sum_array(numbers), 49)

if __name__ == '__main__':
    unittest.main()
