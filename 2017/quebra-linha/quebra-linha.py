import unittest


def quebra_linha(texto, colunas):
    if len(texto) > colunas:
        resultado = ''
        palavras = texto.split(' ')
        resultado = palavras[0]
        palavras = palavras[1:]
        qtd_disponivel = colunas - len(resultado)
        for prox_palavra in palavras:
            if (len(prox_palavra) + 1) <= qtd_disponivel:
                resultado = resultado + ' ' + prox_palavra
                qtd_disponivel -= len(prox_palavra) + 1
            else:
                if qtd_disponivel == 0:
                    resultado += '\n ' + prox_palavra
                    qtd_disponivel = colunas - len(prox_palavra) - 1
                else:
                    resultado += ' \n' + prox_palavra
                    qtd_disponivel = colunas - len(prox_palavra)
        return resultado
    return texto

# FUNCAO NAO TERMINADA
# def quebra_linha(texto, colunas):
#     if len(texto) > colunas:
#         resultado = ''
#         palavras = texto.split(' ')
#         qtd_disponivel = colunas - len(resultado)

#         if len(palavras) > 1:
#             contador = 1
#             for palavra in palavras:
#                 if contador != len(palavras):
#                     if len(palavra) < qtd_disponivel:
#                         resultado = resultado + palavra + " "
#                         qtd_disponivel = qtd_disponivel - len(palavra) - 1
#                     elif len(palavra) == qtd_disponivel:
#                         resultado = resultado + palavra +"\n "
#                         qtd_disponivel = colunas-1
#                     else:
#                         resultado = resultado + "\n" + palavra
#                         if len(palavra)!=colunas:
#                             resultado = resultado+" "
#                         qtd_disponivel = colunas-len(palavra)-1
#                 else:
#                     if len(palavra) <= qtd_disponivel:
#                         resultado = resultado + palavra
#                     else:
#                         resultado = resultado + "\n"+palavra
#                 contador = contador + 1
#         else:
#             resultado = palavras[0]

#         return resultado 

#     return texto


class ExampleTestCase(unittest.TestCase):
    def test_string_smaller_than_columns(self):
        texto = "texto qualquer"
        texto_formatado = quebra_linha(texto, 20)
        self.assertEqual(texto_formatado, "texto qualquer")

    def test_string_bigger_than_columns(self):
        texto = "bom dia"
        texto_formatado = quebra_linha(texto, 4)
        self.assertEqual(texto_formatado, "bom \ndia")

    def test_string_equals_columns(self):
        texto = "bom dor"
        texto_formatado = quebra_linha(texto, 7)
        self.assertEqual(texto_formatado, "bom dor")

    def test_string_bigger_than_columns_2(self):
        texto = "bom dor moreno"
        texto_formatado = quebra_linha(texto, 8)
        self.assertEqual(texto_formatado, "bom dor \nmoreno")

    def test_string_bigger_than_columns_multiple_breaks(self):
        texto = "bom dor dia dor"
        texto_formatado = quebra_linha(texto, 4)
        self.assertEqual(texto_formatado, "bom \ndor \ndia \ndor")

    def test_string_bigger_than_columns_multiple_breaks2(self):
        texto = "boma dor dia dor"
        texto_formatado = quebra_linha(texto, 4)
        self.assertEqual(texto_formatado, "boma\n dor\n dia\n dor")

    def test_string_break_mid_word(self):
        texto = "um pequeno"
        texto_formatado = quebra_linha(texto, 7)
        self.assertEqual(texto_formatado, "um \npequeno")

    def test_string_break_mid_word(self):
        texto = "um pequeno moreno"
        texto_formatado = quebra_linha(texto, 7)
        self.assertEqual(texto_formatado, "um \npequeno\n moreno")

    def test_string_break_morenos_felizes(self):
        texto = "Um pequeno jabuti xereta viu dez morenos felizes."
        texto_formatado = quebra_linha(texto, 20)
        self.assertEqual(texto_formatado, "Um pequeno jabuti \nxereta viu dez \nmorenos felizes.")

    def test_string_break_little_morenos_felize(self):
        texto = "Um pequeno jabuti xereta viu dez morenos felize"
        texto_formatado = quebra_linha(texto, 7)
        self.assertEqual(texto_formatado, "Um \npequeno\n jabuti\n xereta\n viu \ndez \nmorenos\n felize")

if __name__ == '__main__':
    unittest.main()
